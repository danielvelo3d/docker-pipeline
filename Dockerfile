FROM ubuntu:18.04
ENV PATH="/root/.local/bin:${PATH}"

RUN apt update && apt install -y --no-install-recommends \
  autotools-dev \
  ccache \
  cmake \
  dh-autoreconf \
  git \
  g++ \
  gcc \
  mesa-common-dev \
  ninja-build \
  patchelf \
  python3 \
  python3-venv \
  python3-pip \
  python3-setuptools \
  python3-wheel \
  libgl1-mesa-dev libx11-dev xorg-dev\
  && rm -rf /var/lib/apt/lists/*

RUN pip3 install conan